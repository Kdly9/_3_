

#ifndef __MAIN_H
#define __MAIN_H


#include <stdio.h>



  #include "stm324x7i_eval.h"
  #include "stm324x7i_eval_lcd.h"


  #include "stm324x9i_eval.h"
  #include "stm324x9i_eval_lcd.h"



#define USE_LCD

  #define ADCx                     ADC3
  #define ADC_CHANNEL              ADC_Channel_8
  #define ADCx_CLK                 RCC_APB2Periph_ADC3
  #define ADCx_CHANNEL_GPIO_CLK    RCC_AHB1Periph_GPIOF
  #define GPIO_PIN                 GPIO_Pin_10
  #define GPIO_PORT                GPIOF
  #define DMA_CHANNELx             DMA_Channel_2
  #define DMA_STREAMx              DMA2_Stream0
  #define ADCx_DR_ADDRESS          ((uint32_t)0x4001224C)

#endif

#endif /* __MAIN_H */

